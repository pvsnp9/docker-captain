# Docker Captain Commands

## Basics 

`docker version`

`docker info`

`docker`

`docker container run -----`

`docker run`

## Starting a Nginx Web Server

`docker container run --publish 80:80 nginx`

`docker container run --publish 80:80 --detach nginx`

`docker container ls`

`docker container stop 690` - ID first three digit

`docker container ls`

`docker container ls -a`

`docker container run --publish 80:80 --detach --name webhost nginx`

`docker container ls -a`

`docker container logs webhost`

`docker container top` -process stats 

`docker container top webhost`

`docker container --help`

`docker container ls -a`

`docker container rm 63f 690 ode`

`docker container ls`

`docker container rm -f 63f`

`docker container ls -a`

## Container VS. VM: It's Just a Process

`docker run --name mongo -d mongo`

`docker ps`

`docker top mongo`

`docker stop mongo`

`docker start mongo`



## What's Going On In Containers: CLI Process Monitoring

`docker container run -d --name nginx nginx`

`docker container run -d --name mysql -e MYSQL_RANDOM_ROOT_PASSWORD=true mysql`

`docker container ls`

`docker container top mysql`

`docker container top nginx`

`docker container inspect mysql`

`docker container stats --help`

`docker container stats`

`docker container ls`


## Getting a Shell Inside Containers: No Need for SSH

`docker container run -help`

`docker container run -it --name proxy nginx bash`

`docker container ls`


`docker container run -it --name ubuntu ubuntu`


`docker container start --help`

`docker container start -ai ubuntu` - Attach STDOUT/STDERR and forward signals, Attach container’s STDIN

`docker container exec --help`

`docker container exec -it mysql bash`

`docker pull alpine`

`docker image ls`

`docker container run -it alpine bash`

`docker container run -it alpine sh`

## Docker Networks: Concepts for Private and Public Comms in Containers

`docker container run -p 80:80 --name webhost -d nginx`

`docker container port webhost`

`docker container inspect --format '{{ .NetworkSettings.IPAddress }}' webhost`

## Docker Networks: CLI Management of Virtual Networks

`docker network ls`

`docker network inspect bridge`

`docker network create my_app_net`

`docker network create --help`

`docker container run -d --name new_nginx --network my_app_net nginx` - attachine container to network

`docker network inspect my_app_net`

`docker network connect`

`docker container inspect <container>`

`docker container disconnect <container>`

`docker container inspect`

## Docker Networks: DNS and How Containers Find Each Other

`docker network inspect <container>`

`docker container run -d --name my_nginx --network my_app_net nginx`

`docker container inspect <container>`

`docker container exec -it my_nginx ping new_nginx`

`docker container exec -it new_nginx ping my_nginx`

`docker container create --help`



##  DNS Round Robin Testing

`docker network create dude`

`docker container run -d --net dude --net-alias search elasticsearch:2`

`docker container ls`

`docker container run --rm -- net dude alpine nslookup search`

`docker container run --rm --net dude centos curl -s search:9200`


`docker container rm -f TAB <container>`


# Container Images, Where To Find Them and How To Build Them

## The Mighty Hub: Using Docker Hub Registry Images

`docker image ls`

`docker pull nginx`

`docker pull nginx:1.11.9`

`docker pull nginx:1.11`

`docker pull nginx:1.11.9-alpine`

`docker image ls`

## Images and Their Layers: Discover the Image Cache

`docker image ls`

`docker history nginx:latest`

`docker history mysql`

`docker image inspect nginx`

## Image Tagging and Pushing to Docker Hub

`docker image tag -- help`

`docker image ls`

`docker pull mysql/mysql-server`

`docker image ls`

`docker pull nginx:mainline`

`docker image ls`

`docker image tag nginx name/nginx`

`docker image tag --help`

`docker image ls`

`docker image push name/nginx`



## Building Images: Running Docker Builds

`docker image build -t customnginx .`

`docker image ls`

`docker image build -t customnginx .`

## Building Images: Extending Official Images

`cd dockerfile-sample-2`

`vim Dockerfile`

`docker container run -p 80:80 --rm nginx`

`docker image build -t nginx-with-html .`

`docker container run -p 80:80 --rm nginx-with-html`

`docker image ls`

`docker image tag --help`

`docker image tag nginx-with-html:latest name/nginx-with-html:latest`

`docker image ls`

`docker push`

## Build Your Own Dockerfile and Run Containers From It

`cd dockerfile-assignment-1`

`vim Dockerfile`

`docker build cmd`

`docker build -t testnode .`

`docker container run --rm -p 80:3000 testnode`

`docker images`

`docker tag --help`

`docker tag testnode name/testing-node`

`docker push --help`

`docker push name/testing-node`

`docker image ls`

`docker image rm name/testing-node`

`docker container run --rm -p 80:3000 name/testing-node`


# Container Lifetime & Persistent Data: Volumes, Volumes, Volumes

## Persistent Data: Data Volumes

`docker pull mysql`

`docker image inspect mysql`

`docker container run -d --name mysql -e MYSQL_ALLOW_EMPTY_PASSWORD=True mysql`

`docker container ls`

`docker container inspect mysql`

`docker volume ls`

`docker volume inspect <volume>`

`docker container run -d --name mysql2 -e MYSQL_ALLOW_EMPTY_PASSWORD=True mysql`

`docker volume ls`

`docker container stop mysql`

`docker container stop mysql2`

`docker container ls`

`docker container ls -a`

`docker volume ls`

`docker container rm mysql mysql2`


`docker container run -d --name mysql -e MYSQL_ALLOW_EMPTY_PASSWORD=True -v mysql-db:/var/lib/mysql mysql` - Specifying named volume


`docker volume ls`

`docker volume inspect mysql-db`

`docker container rm -f mysql`

`docker container run -d --name mysql3 -e MYSQL_ALLOW_EMPTY_PASSWORD=True -v mysql-db:/var/lib/mysql mysql`

`docker volume ls`

`docker container inspect mysql3`

`docker volume create --help`

## Persistent Data: Bind Mounting

*Maps a host file or directory to a container file or directory. - only in docker run*

`run -v /user/name/stuff: /path/container`

`cd dockerfile-sample-2`

`pcat Dockerfile`

`docker container run -d --name nginx -p 80:80 -v $(pwd):/usr/share/nginx/html nginx` -bind mount command


`docker container run -d --name nginx2 -p 8080:80 nginx`

`docker container exec -it nginx bash`

## Database upgrade with containers

`docker volume create psql`

`docker run -d --name psql1 -e POSTGRES_PASSWORD=mypassword -v mydb:/var/lib/postgresql/data postgres:15.1`

`docker logs psql1`  `-f` t-o follow or watch logs

`docker stop psql1`

`docker run -d --name psql2 -e POSTGRES_PASSWORD=mypassword -v mydb:/var/lib/postgresql/data postgres:15.2`

`docker logs psql2`

`docker stop psql2`


## Edit Code Running In Containers With Bind Mounts

`docker run -p 80:4000 -v $(pwd):/site name/jekyll-serve`



# Making It Easier with Docker Compose: The Multi-Container Tool

## Docker Compose and The Docker-compose.yml File

`docker-compose.yml`

## Trying Out Basic Compose Commands

`pcat docker-compose.yml`

`docker-compose up`

`docker-compose up -d`

`docker-compose logs`

`docker-compose --help`

`docker-compose ps`

`docker-compose top`

`docker-compose down`

## Build a Compose File for a Multi-Container Service

`docker-compose.yml`

`docker pull drupal`

`docker image inspect drupal`

`docker-compose up`


`docker-compose down --help`

`docker-compose down -v` -  Remove named volumes declared in the volumes section of the Compose file and anonymous volumes attached to containers.

## Adding Image Building to Compose Files

`docker-compose.yml`

`docker-compose up`

`docker-compose up --build`

`docker-compose down`

`docker image ls`

`docker-compose down --help`

`docker image rm nginx-custom`

`docker image ls`

`docker-compose up -d`

`docker image ls`


`docker-compose down --rmi local`

## Compose for Run-Time Image Building and Multi-Container Dev

`docker-compose up`

`docker-compose down`

`docker-compose up`


# Swarm Basic Features and How to Use Them In Your Workflow

## Scaling Out with Overlay Networking

`docker network create --driver overlay mydrupal`

`docker network ls`

`docker service create --name psql --network mydrupal -e POSTGRES_PASSWORD=mypass postgres`

`docker service ls`

`docker service ps psql`

`docker container logs psql <containername>`

`docker service create --name drupal --network mydrupal -p 80:80 drupal`

`docker service ls`

`watch docker service ls`

`docker service ps drupal`

`docker service inspect drupal`

## Scaling Out with Routing Mesh

`docker service create --name search --replicas 3 -p 9200:9200 elasticsearch:2`

`docker service ps search`

## Create a Multi-Service Multi-Node Web App

`docker node ls`

`docker service ls`

`docker network create -d overlay backend`

`docker network create -d overlay frontend`

`docker service create --name vote -p 80:80 --network frontend -- replica 2 COPY IMAGE`

`docker service create --name redis --network frontend --replica 1 redis:3.2`

`docker service create --name worker --network frontend --network backend COPY IMAGE`

`docker service create --name db --network backend COPY MOUNT INFO`

`docker service create --name result --network backend -p 5001:80 COPY INFO`

`docker service ls`

`docker service ps result`

`docker service ps redis`

`docker service ps db`

`docker service ps vote`

`docker service ps worker`

`cat /etc/docker/`

`docker service logs worker`

`docker service ps worker`

## Swarm Stacks and Production Grade Compose

`docker stack deploy -c example-voting-app-stack.yml voteapp`

`docker stack`

`docker stack ls`

`docker stack ps voteapp`

`docker container ls`

`docker stack services voteapp`

`docker stack ps voteapp`

`docker network ls`

`docker stack deploy -c example-voting-app-stack.yml voteapp`

## Using Secrets in Swarm Services

`docker secret create psql_usr psql_usr.txt`

`echo "myDBpassWORD" | docker secret create psql_pass - TAB COMPLETION`

`docker secret ls`

`docker secret inspect psql_usr`

`docker service create --name psql --secret psql_user --secret psql_pass -e POSTGRES_PASSWORD_FILE=/run/secrets/psql_pass -e POSTGRES_USER_FILE=/run/secrets/psql_user postgres`

`docker service ps psql`

`docker exec -it psql.1.CONTAINER NAME bash`

`docker log` 

`docker service ps psql`

`docker service update --secret-rm`

## Using Secrets with Swarm Stacks

`vim docker-compose.yml`

`docker stack deploy -c docker-compose.yml mydb`

`docker secret ls`

`docker stack rm mydb`

## Create A Stack with Secrets and Deploy

`vim docker-compose.yml`

`docker stack deploy - c docker-compose.yml drupal`

`echo STRING |docker secret create psql-ps - VALUE`

`docker stack deploy -c docker-compose.yml drupal`

`docker stack ps drupal`

# Swarm App Lifecycle

## Using Secrets With Local Docker Compose

`docker node ls`

`docker-compose up -d`

`docker-compose exec psql cat /run/secrets/psql_user`

`docker-compose 11`

`pcat docker-compose.yml`

## Full App Lifecycle: Dev, Build and Deploy With a Single Compose Design

`docker-compose up -d`

`docker inspect`

`docker-compose down`

`docker-compose -f docker-compose.yml -f docker-compose.test.yml up -d`

`docker inspect`

`docker-compose -f docker-compose.yml -f docker-compose.prod.yml config --help`

`docker-compose -f docker-compose.yml -f docker-compose.prod.yml config`

`docker-compose -f docker-compose.yml -f docker-compose.prod.yml config > output.yml`

## Service Updates: Changing Things In Flight

`docker service create -p 8088:80 --name web nginx:1.13.7`

`docker service ls`

`docker service scale web=5`

`docker service update --image nginx:1.13.6 web`

`docker service update --publish-rm 8088 --publish-add 9090:80`

`docker service update --force web`

## Healthchecks in Dockerfiles

`docker container run --name p1 -d postgres`

`docker container ls`

`docker container run --name p2 -d --health-cmd="pg_isready -U postgres || exit 1" postgres`

`docker container ls`

`docker container inspect p2`

`docker service create --name p1 postgres`

`docker service create --name p2 --health-cmd="pg_isready -U postgres || exit 1" postgres`

# Container Registries: Image Storage and Distribution

### Docker Hub: Digging Deeper

https://hub.docker.com

### Docker Store: What Is It For?

https://store.docker.com

### Docker Cloud: CI/CD and Server Ops

https://cloud.docker.com

https://hub.docker.com

### Understanding Docker Registry

https://github.com/docker/distribution

https://hub.docker.com/registry

## Run a Private Docker Registry

`docker container run -d -p 5000:5000 --name registry registry`

`docker container ls`

`docker image ls`

`docker pull hello-world`

`docker run hello-world`

`docker tag hello-world 127.0.0.1:5000/hello-world`

`docker image ls`

`docker push 127.0.0.1:5000/hello-world`

`docker image remove hello-world`

`docker image remove 127.0.0.1:5000/hello-world`

`docker container rm admiring_stallman`

`docker image remove 127.0.0.1:5000/hello-world`

`docker image ls`

`docker pull 127.0.0.1:5000/hello-world:latest`

`docker container kill registry`

`docker container rm registry`

`docker container run -d -p 5000:5000 --name registry -v $(pwd)/registry-data:/var/lib/registry registry <container_name>`

`docker image ls`

`docker push 127.0.0.1:5000/hello-world`

## Using Docker Registry With Swarm

http://play-with-docker.com

`docker node ls`

`docker service create --name registry --publish 5000:5000 registry`

`docker service ps registry`

`docker pull hello-world`

`docker tag hello-world 127.0.0.1:5000/hello-world`

`docker push 127.0.0.1:5000/hello-world`

`docker pull nginx`

`docker tag nginx 127.0.0.1:5000/nginx`

`docker push 127.0.0.1:5000/nginx`

`docker service create --name nginx -p 80:80 --replicas 5 --detach=false 127.0.0.1:5000/nginx`

`docker service ps nginx`



# Kubernetes Install and Your First Pods

## Kubernetes Local Install

http://play-with-k8s.com

katacoda.com

### minikube

minikube-installer.exe

minikube start

### microk8s

`microk8s.kubectl`

`microk8s.enable dns`

`alias kubectl=microk8s.kubectl`

## Kubectl run, create and apply

`kubectl run`

`kubectl create`

`kubectl apply`

## Our First Pod With Kubectl run

`kubectl version`

`kubectl run my-nginx --image nginx`

`kubectl get pods`

`kubectl get all`

`kubectl delete deployment my-nginx`

`kubectl get all`

## Scaling ReplicaSets

`kubectl run my-apache --image httpd`

`kubectl get all`

`kubectl scale deploy/my-apache --replicas2`

`kubectl scale deployment my-apache --replicas2`

`kubectl get all`

## Inspecting Kubernetes Objects

`kubectl get pods`

`kubectl logs deployment/my-apache`

`kubectl logs deployment/my-apache --follow --tail 1`

`kubectl logs -l run=my-apache`

`kubectl get pods`

`kubectl describe pod/my-apache-<pod id>`

`kubectl get pods -w`

`kubectl delete pod/my-apache-<pod id>`

`kubectl get pods`

`kubectl delete deployment my-apache`


# Exposing Kubernetes Ports

## Service Types

`kubectl expose`

## Creating a ClusterIP Service

`kubectl get pods -w`

`kubectl create deployment httpenv --image=name/httpenv`

`kubectl scale deployment/httpenv --replicas=5`

`kubectl expose deployment/httpenv --port 8888`

`kubectl get service`

`kubectl run --generator run-pod/v1 tmp-shell --rm -it --image name/netshoot -- bash`

`curl httpenv:8888`

`curl [ip of service]:8888`

`kubectl get service`

## Creating a NodePort and LoadBalancer Service

`kubectl get all`

`kubectl expose deployment/httpenv --port 8888 --name httpenv-np --type NodePort`

`kubectl get services`

`curl localhost:32334`

`kubectl expose deployment/httpenv --port 8888 --name httpenv-lb --type LoadBalancer`

`kubectl get services`

`curl localhost:8888`

`kubectl delete service/httpenv service/httpenv-np`

`kubectl delete service/httpenv-lb deployment/httpenv`

## Kubernetes Services DNS

`curl <hostname>`

`kubectl get namespaces`

`curl <hostname>.<namespace>.svc.cluster.local`

# Kubernetes Management Techniques

## Run, Expose and Create Generators

`kubectl create deployment sample --image nginx --dry-run -o yaml`

`kubectl create deployment test --image nginx --dry-run`

`kubectl create deployment test --image nginx --dry-run -o yaml`

`kubectl create job test --image nginx -dry-run -o yaml`

`kubectl expose deployment/test --port 80 --dry-run -o -yaml`

`kubectl create deployment test --image nginx`

`kubectl expose deployment/test --port 80 --dry-run -o -yaml`

`kubectl delete deployment test`

## The Future of Kubectl Run

`kubectl run test --image nginx --dry-run`

`kubectl run test --image nginx --port 80 --expose --dry-run`

`kubectl run test --image nginx --restart OnFailure --dry-run`

`kubectl run test --image nginx --restart Never --dry-run`

`kubectl run test --image nginx --scheduled "*/1 * * * *" --dry-run`

## Imperative vs. Declarative

`kubectl apply -f my-resources.yaml`

`kubectl run`

# Moving to Declarative Kubernetes YAML

## Kubectl Apply

`kubectl apply -f filename.yml`

`kubectl apply -f myfile.yaml`

`kubectl apply -f myyaml/`



## Building Your YAML Files

`kubectl api-resources`

`kubectl api-versions`

## Dry Runs and Diffs

`kubectl apply -f app.yml --dry-run`

`kubectl apply -f app.yml --server-dry-run`

`kubectl diff -f app.yml`

## Labels and Label Selectors

`kubectl get pods -l app=nginx`

`kubectl apply -f myfile.yaml -l app=nginx`

`kubectl get all`

`kubectl delete <resource type>/<resource name>`


# Next Steps, and The Future of Kubernetes

## Kubernetes Dashboard

https://github.com/kubernetes/dashboard

## Namespaces and Context

`kubectl get namespaces`

`kubectl get all -all-namespaces`

`~/.kube/config file`

`kubectl config get-contexts`

`kubectl config set*`

















 




